# make IonizeCell
# make IonizeCell MYFLAGS=-DVERBOSE

CC = gcc
FC = gfortran
VERB_FLAGS = -Wall
OPT_FLAGS = -O3 -flto
#DEBUG_FLAGS = -DDEBUG -O0 -g
CFLAGS = $(VERB_FLAGS) $(OPT_FLAGS) $(DEBUG_FLAGS)
FFLAGS = $(CFLAGS)
LD = $(FC)
LDFLAGS = $(CFLAGS)
AR = gcc-ar

INDENTFLAGS = --no-tabs --break-function-decl-args 

# For verbose output
#DEFS += -DVERBOSE

LIBATOMIC = AtomicDataLib/libatomic.a

CSRC = Atom_rates.c \
       checkFractions.c \
       Euler_loop.c \
       fdot.c \
       IonizeCell.c

EXEC = IonizeCell

OBJS = $(CSRC:.c=.o)

all : $(EXEC)

$(EXEC) : $(OBJS) $(LIBATOMIC)
	$(LD) $(LDFLAGS) -o $(EXEC) $(OBJS) $(LIBATOMIC) $(LIBS)

$(LIBATOMIC):
	$(MAKE) CC=$(CC) FC=$(FC) CFLAGS="$(CFLAGS)" FFLAGS="$(FFLAGS)" DEFS=$(DEFS) AR=$(AR) -C AtomicDataLib

.PHONY: distclean

distclean: clean
	-rm -f $(EXEC)

.PHONY: clean
clean :
	-rm -f $(OBJS)
	-rm -f *\~
	$(MAKE) -C AtomicDataLib clean

.PHONY: check
check:
	$(MAKE) -C tests

.SUFFIXES: .o .c

.PHONY: indent
indent:
	indent $(INDENTFLAGS) $(CSRC) *.h
	$(MAKE) INDENTFLAGS="$(INDENTFLAGS)" -C AtomicDataLib indent

.c.o:
	$(CC) $(CFLAGS) $(DEFS) $(INCS) -c $<
