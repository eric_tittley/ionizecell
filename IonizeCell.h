#ifndef _IONIZE_CELL_H_
#define _IONIZE_CELL_H_

void checkFractions(
 double **const f,
 const int sizeZ,
 int const *const J,
 const char *FILE,
 const int LINE);

#endif
