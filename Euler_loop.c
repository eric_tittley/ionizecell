/* A simple Eulerian forward time-stepping scheme. The number of time steps
 * is limited by maxtsteps. Separate data dumps are output at numdumps specified
 * times dumptimes into files dumpfilenames.
 * -------------------------------------------------------------------------
 * -------------------------------------------------------------------------
 * Authors:             Avery Meiksin    (IfA, U Edinburgh)
 *                      Simon Reynolds   (IfA, U Edinburgh, for PhD)
 * Written:             2007-2009        (as part of timedep_* series)
 * Modified:            10-Jul-2013      (cleaned up by AM)
 *
 * -------------------------------------------------------------------------
 * Called by IonizeCell.c.
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "NumericalTolerances.h"

static double dmaxarg1, dmaxarg2;
#define DMAX(a,b) (dmaxarg1=(a),dmaxarg2=(b),(dmaxarg1) > (dmaxarg2) ?\
        (dmaxarg1) : (dmaxarg2))        /* from nrutil.h */

extern double calcfdot(
 double **f,
 double **fdot,
 double **c_H,
 double **c_He,
 double **r_H,
 double **r_He,
 double **r,
 double **c,
 double **d,
 double **a,
 double **p,
 double ***q,
 double *n_e,
 int sizeZ,
 double *nTot,
 int *J,
 double timescale);

extern void calc_rates(
 double **c_H,
 double **c_He,
 double **r_H,
 double **r_He,
 double **r,
 double **c,
 double **d,
 double **a,
 double **p,
 double ***q,
 double ****photoprob,
 int sizeZ,
 int *Z,
 int *J,
 int maxelecs,
 int num_quadr,
 double J_L,
 float Temp,
 double R_0,
 double *ColDen,
 int LIGHT,
 double redshift);

double timeloop(
 double **f,
 double **fdot,
 double **c_H,
 double **c_He,
 double **r_H,
 double **r_He,
 double **r,
 double **c,
 double **d,
 double **a,
 double **p,
 double ***q,
 double ****photoprob,
 double *n_e,
 double t_start,
 int sizeZ,
 int *Z,
 double *nTot,
 double *metallis,
 int *J,
 int maxelecs,
 int num_quadr,
 float Temp,
 double timescale,
 FILE * outfile,
 int maxtsteps,
 double dumptimes[],
 char dumpfilenames[][70],
 int numdumps)
{
 double t = t_start;
 double deltat;
#ifdef VERBOSE
 double fracsum;
#endif
 int i, j;
 FILE *fdump;

 /* Time variables for deciding when to write fractions to file */
 int ntstep = 0;
 int ndump = 0;
 float t_stepout = 2.e11;
 float t_nextout, t_startout;
 t_startout = 0.0;
 if (numdumps < 1) {
  t_nextout = t_startout + t_stepout;
 } else {
  t_nextout = t_startout + dumptimes[ndump];
 }

/********************************** START OF TIMESTEP **********************/
 /* CALL THE FDOT FUNCTION TO FIND ALL DERIVATIVES. */
 /* Arrays passed by ref (their names are pointers). Density terms,
  * nTot[] and J[] are passed by ref. sizeZ and timescale passed by value;
  * timescale is returned. */
 while (ntstep < maxtsteps) {
  fflush(stdout);
  timescale = calcfdot(f,
                       fdot,
                       c_H,
                       c_He,
                       r_H,
                       r_He, r, c, d, a, p, q, n_e, sizeZ, nTot, J, timescale);

  if (timescale == 0.0) {
   deltat = 1.0e14;             /* Just trying to skip towards the next dump */
   printf("EULER_LOOP: timescale = 0.0  ,  Setting deltat = 1.0e14s \n");
  } else {
   deltat = 0.1 * timescale;
   if (deltat > 1.e14)
    deltat = 1.e14;
  }

#ifdef VERBOSE
  printf("timescale= %e\n", timescale);
  printf("%e\t%e\t%e\t%e\t%e\t\t%e\t%e\t%e\t%e\t\tsizeZ = %d\t\tdeltat = %e\n",
         t, f[1][1], f[2][1], f[3][1], f[4][1], fdot[1][1], fdot[2][1],
         fdot[3][1], fdot[4][1], sizeZ, deltat);
//printf("%e\t%e\t%e\t%e\n",f[1][1],f[2][1],f[3][1],f[4][1]);
#endif

  for (i = 1; i < sizeZ; ++i) {
   for (j = 1; j <= J[i]; ++j) {
    f[i][j] += fdot[i][j] * deltat;     /* UPDATE ION FRACTIONS */

    /* These rounding tests are time expensive. */
    /* Rounding if get -ve or over 1 by 1e-7 and exiting for total
     * being off by 1e-6. */
    if (f[i][j] >= 1.0) {
     f[i][j] = 1.0;
#ifdef VERBOSE
     printf("Rounded f[%d][%d] down to 1.0.\n", i, j);
#endif
    }
    if (f[i][j] < VERY_SMALL_NUMBER) {
     f[i][j] = VERY_SMALL_NUMBER;
#ifdef VERBOSE
     printf("Zeroed f[%d][%d].\n", i, j);
#endif
    }

   }
  }
#ifdef VERBOSE
  /* Check ion fractions still sum to 1 */
  for (i = 1; i < sizeZ; ++i) {
   fracsum = 0.0;
   for (j = 1; j <= J[i]; ++j) {
    fracsum += f[i][j];
   }
   if (fabs(fracsum - 1.0) > 1.0e-6) {  /*if sum of fractions != (1+-0.000001) */
    //      printf("Ion fractions for element %d do not sum to 1.0\n ... exiting to system\n",i);
    printf("EULER_LOOP WARNING: Ion fractions for element %d sum to %f\n", i,
           fracsum);
    for (j = 1; j <= J[i]; ++j) {
     printf("%d  %e\n", j, f[i][j]);
    }
    //      exit(1);
   }
  }
#endif

  /* Increment time */
  t += deltat;
  ntstep += 1;

      /******* WRITE FRACTIONS TO FILE  *******/
  /* ...when t reaches the time-for-next-output */
  if (t >= t_nextout || (t + deltat) >= dumptimes[numdumps - 1]) {      /*added to print on final loop */
   if (numdumps <= 0) {
    t_nextout = t_startout + pow(1.1, ntstep) * t_stepout;
   } else {
    if (ndump < numdumps) {
     if ((fdump = fopen(dumpfilenames[ndump], "w")) == NULL) {
      printf("EULER_LOOP: Can't open time dump file...exiting\n");
      exit(1);
     }
     for (i = 1; i < sizeZ; ++i) {
      for (j = 1; j <= J[i]; ++j) {
       /* Format to copy Cloudy's output. */
       fprintf(fdump, "%.3f ", DMAX(-30.0, log10(f[i][j])));
      }
      fprintf(fdump, "\n");
     }
     if (fclose(fdump) != 0) {
      printf("EULER_LOOP: Can't close output time dump file...exiting\n");
      exit(1);
     }
     ndump++;
     if (ndump < numdumps)
      t_nextout = dumptimes[ndump];
    }
   }

#ifdef VERBOSE
   fprintf(outfile, "%e", t);
   for (i = 1; i < sizeZ; ++i) {
    for (j = 1; j <= J[i]; ++j) {
     fprintf(outfile, "\t%.4e", f[i][j]);

     if (fflush(outfile) != 0) {
      printf("fflush failed\nExiting.\n");
      exit(1);
     }

    }
   }
   fprintf(outfile, "\t%e\n", *n_e);
#endif

  }
 }

 /******** END OF TIME LOOP *********/
 return t;
}

/*---------------------------------------------------------*/
