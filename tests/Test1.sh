#!/bin/bash

cd 1
../../IonizeCell lya_Up01_a1p5_CSi.in >> /dev/null
diff -q final_fracs_compare.dat final_fracs.dat && set state=0 || exit 1

rm file_1e13.out  file_1e14.out  file_1e15.out  file_1e16.out final_fracs.dat
