      subroutine myeion(nz,ne,is,ei)
********************************************
*** Written (badly) by scr 06,03,06
*** Pulls the threshold ionisation energy ei
*** (eV?) out of the data table ph1.  
********************************************
      integer nz,ne,is
      real ei
      common/ph1/ph1(6,30,30,7)
      ei=0.0
      ei=ph1(1,nz,ne,is)
      return
      end
********************************************
