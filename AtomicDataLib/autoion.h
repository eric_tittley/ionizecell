#ifndef _AUTOION_H_
#define _AUTOION_H_

#define MAX_PATH_LENGTH 256

/* autoion: scr 23.03.06                                   **
** Autoionisation rates from Arnaud & Rothenflug 1985.     */

/*---------------------------------------------------------**
** ARf1: Arnaud&Rothenflug85's analytical approximations   **
** for the intergral f1(x) given in their Appendix B.      */
float ARf1(float x)
{
 if (x <= 0.02) {
  if (x <= 0.0)
   printf("ERROR: negative arguement for ARf1. x=%lf\n", x);
  return exp(x) * (-log(x) - 0.5772 + x);
 }

 if (x >= 10.0) {
  return (1 - (1 / x) + (2 / (x * x)) - (6 / pow(x, 3)) + (24 / pow(x, 4))) / x;
 }

 if (x > 0.02 && x < 1.5) {
  return log((x + 1) / x) - (0.36 +
                             0.03 * pow((x + 0.01),
                                        -0.5)) / ((x - 1) * (x - 1));
 }

 if (x >= 1.5 && x < 10.0) {
  return log((x + 1) / x) - (0.36 +
                             0.03 * pow((x + 0.01), 0.5)) / ((x - 1) * (x - 1));

 } else {
  printf("ERROR in autoion.h : arguement for ARf1 x=%lf\n", x);
  //exit(1);
  return 1.0;
 }
}


/*-----------------------------------------------------**
** Autoionisation rates from Arnaud & Rothenflug 1985. **
** Temp must be given in Kelvin!                       */
double autoion(int Z, int numelecs, float Temp)
{
 double I_ea, y, f1;
 double x, b, G;
 double aI, F;

 /* Boltzmann's Constant 
  * k = 8.617339 � 10^-5 electron-volts/kelvin. */
 double kT = 8.617339e-5 * Temp;

 switch (numelecs) {
  case 3:

   switch (Z) {
    case 6:
     x = 0.6;
     break;
    case 7:
     x = 0.8;
     break;
    case 8:
     x = 1.25;
     break;
    default:
     x = 1.2;
   }

   I_ea = 13.6 * ((Z - 0.835) * (Z - 0.835) - 0.25 * (Z - 1.62) * (Z - 1.62));
   y = I_ea / kT;
   f1 = ARf1(y);
   b = 1 / (1 + 2.0e-4 * Z * Z * Z);
   G = 2.22 * f1 + 0.67 * (1 - y * f1) + 0.49 * y * f1 + 1.2 * y * (1 - y * f1);
   return x * 1.6e-7 * b * 1.2 * exp(-y) * G / ((Z - 0.43) * (Z - 0.43) *
                                                sqrt(kT));
   printf("autoion: Shouldn't be here!\n");     /*test */
   break;

  case 11:
   if (Z <= 16 && Z != 11) {    /*scr added !=11, since pow((11-11),-0.7)=inf */

    I_ea = 26 * (Z - 10);
    aI = 2.8e-17 * pow((Z - 11), -0.7) * I_ea;
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 - y * f1;

   } else {
    if (Z >= 18 && Z <= 28) {

     I_ea = 11 * pow((Z - 10), 1.5);
     aI = 1.3e-14 * pow((Z - 10), -3.73) * I_ea;
     y = I_ea / kT;
     f1 = ARf1(y);
     F = 1 - 0.5 * (y - y * y + y * y * y * f1);

    } else {                    /* for all other Z */
     return 0.0;
    }
   }
   break;

  case 12:
   if (Z >= 18) {

    I_ea = 10.3 * pow((Z - 10), 1.52);
    aI = 4.0e-13 * pow(Z, -2);
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 - 0.5 * (y - y * y + y * y * y * f1);

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  case 13:
   if (Z >= 18) {

    I_ea = 18.0 * pow((Z - 11), 1.33);
    aI = 4.0e-13 * pow(Z, -2);
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 - 0.5 * (y - y * y + y * y * y * f1);

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  case 14:
   if (Z >= 18) {

    I_ea = 18.4 * pow((Z - 12), 1.36);
    aI = 4.0e-13 * pow(Z, -2);
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 - 0.5 * (y - y * y + y * y * y * f1);

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  case 15:
   if (Z >= 18) {

    I_ea = 23.7 * pow((Z - 13), 1.29);
    aI = 4.0e-13 * pow(Z, -2);
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 - 0.5 * (y - y * y + y * y * y * f1);

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  case 16:
   if (Z >= 18) {

    I_ea = 40.1 * pow((Z - 14), 1.10);
    aI = 4.0e-13 * pow(Z, -2);
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 - 0.5 * (y - y * y + y * y * y * f1);

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  case 19:                     /* CaII */
   if (Z == 20) {

    I_ea = 29;
    aI = 9.8e-17 * I_ea;
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 + 1.12 * f1;

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  case 20:                     /* CaI */
   if (Z == 20) {

    I_ea = 25;
    aI = 6.0e-17 * I_ea;
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 + 1.12 * f1;

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  case 22:                     /* FeV */
   if (Z == 26) {

    I_ea = 73;
    aI = 5.0e-18 * I_ea;
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 + f1;

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  case 23:                     /* FeIV */
   if (Z == 26) {

    I_ea = 60;
    aI = 1.8e-17 * I_ea;
    y = I_ea / kT;
    f1 = ARf1(y);
    F = 1 + f1;

   } else {                     /* for all other Z */
    return 0.0;
   }
   break;

  default:                     /* for all other numelecs */
   return 0.0;
 }

 if (numelecs != 11 &&
     numelecs != 12 &&
     numelecs != 13 &&
     numelecs != 14 &&
     numelecs != 15 &&
     numelecs != 16 &&
     numelecs != 19 && numelecs != 20 && numelecs != 22 && numelecs != 23) {
  printf("ERROR in autoion.h : numelecs=%d\n", numelecs);
  exit(1);
 }

 /* Sequences 11,12-16,19,20,22 and 23 share this formula */
 //  printf("Auto(Z=%d,#=%d): 6.69e7 * %e * %e * %e / %e\n",Z,numelecs,aI,exp(-y),F,sqrt(kT));
 return 6.69e7 * aI * exp(-y) * F / sqrt(kT);
}

#endif
