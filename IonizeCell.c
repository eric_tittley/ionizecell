/* Evolves hydrogen, helium and metal ionization fractions.
 * -------------------------------------------------------------------------
 * -------------------------------------------------------------------------
 * Authors:             Avery Meiksin    (IfA, U Edinburgh; A.Meiksin@ed.ac.uk)
 *                      Simon Reynolds   (IfA, U Edinburgh, for PhD)
 * Written:             2007-2009        (as timedep_* series)
 * Modified:            10-Jul-2013      (cleaned up by AM; free-standing)
 *
 * Requirements: Atomic_rates.c, Euler_loop.c, fdot.c; atomic data files and
 *               reading routines in subdirectory AtomicData of src directory.
 * ----------------------------------------------------------------
 * Notation mostly follows Verner & Iakovlev(Yakovlev) 1990
 * Astrophysics and Space Science 165:27-40.
 * Equation numbers referenced in the comments are from there.
 * Note error in q_ijk, as corrected in eqs.(49) and (50) of Meiksin 2009,
 * Rev Mod Phys 81:1405-1469.
 * N.B. Metal arrays start with element 1 (not 0).
 *
 * -------------------------------------------------------------------------
*/

#include "IonizeCell.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

static double dmaxarg1, dmaxarg2;
#define DMAX(a,b) (dmaxarg1=(a),dmaxarg2=(b),(dmaxarg1) > (dmaxarg2) ?\
        (dmaxarg1) : (dmaxarg2))        /* from nrutil.h */

extern void calc_rates(
 double **c_H,
 double **c_He,
 double **r_H,
 double **r_He,
 double **r,
 double **c,
 double **d,
 double **a,
 double **p,
 double ***q,
 double ****photoprob,
 int sizeZ,
 int *Z,
 int *J,
 int maxelecs,
 int num_quadr,
 double J_L,
 float Temp,
 double R_0,
 double *ColDen,
 double redshift);

double **memarray2(
 double **arraynm,
 int sizeZ,
 int *J);
double ***memarray3(
 double ***arraynm,
 int sizeZ,
 int *J,
 int maxnull);
double ****memarray4(
 double ****arraynm,
 int sizeZ,
 int *J,
 int maxnull,
 int kmax,
 int lmax);
void freemem(
 double *arraynm);
void freemem2(
 double **arraynm);
void freemem3(
 double ***arraynm);

extern double timeloop(
 double **f,
 double **fdot,
 double **c_H,
 double **c_He,
 double **r_H,
 double **r_He,
 double **r,
 double **c,
 double **d,
 double **a,
 double **p,
 double ***q,
 double ****photoprob,
 double *n_e,
 double t_start,
 int sizeZ,
 int *Z,
 double *nTot,
 double *metallis,
 int *J,
 int maxelecs,
 int num_quadr,
 float Temp,
 double timescale,
 FILE * outfile,
 int maxtsteps,
 double dumptimes[],
 char dumpfilenames[][70],
 int numdumps);

int main(
 int argc,
 char *argv[])
{

 int i, j;
 FILE *param_file;

 /*This program is called with the name of the input file as an argument */
 if ((param_file = fopen(argv[1], "r")) == NULL) {
  printf("Unable to open %s. Exiting to system..\n", argv[1]);
  exit(1);
 }

 double dummy_d = 0.0;
 double *dummy_ptd = NULL;

    /**********************************************/

 /* time variables, incl adaptive timestep */
 float t_start = 0.0;
 double t = t_start;
 double timescale;

 double J_L;

 FILE *outfile;
 FILE *outfile2;

 /* Read in input file with nTot[],sizeZ,Z[], (J[]) , ALL PADDED WITH A LEADING ZERO */
 int sizeZ;                     /*sizeZ = # of metals + 2 (i.e. H and He) + 1 (padding) */
 int num_quadr, numdumps, maxtsteps;
 float Temp;

 if (fscanf(param_file, "sizeZ= %d\n", &sizeZ) != 1) {
  printf("Error reading sizeZ\n");
  exit(1);
 }
 int Z[sizeZ];                  /* Atomic number of each element */
 int J[sizeZ];                  /* Maximum ionisation state to include for each element.
                                 * To include all states we use J[i]=Z[i]+1  (eqn 2) */
 /* Metallicities as a fraction of the hydrogen number density */
 double metallis[sizeZ];
 /* Total number density of each element (n_i in eqn 3) */
 double nTot[sizeZ];
 Z[0] = 0;
 J[0] = 0;
 nTot[0] = 0.0;

 if (fscanf(param_file, "num_quadr= %d\n", &num_quadr) != 1) {
  printf("Error reading num_quadr. sizeZ=%d\n", sizeZ);
  exit(1);
 }

 if (fscanf(param_file, "Temp= %f\n", &Temp) != 1) {    /* Temp in Kelvin */
  printf("Error reading Temp. sizeZ=%d, num_quadr=%d\n", sizeZ, num_quadr);
  exit(1);
 }
 /* Total number density of hydrogen (in cm^-3) */
 if (fscanf(param_file, "n_H=%lf\n", &nTot[1]) != 1) {
  printf("Error reading n_H. sizeZ=%d, num_quadr=%d,Temp=%f\n", sizeZ,
         num_quadr, Temp);
  exit(1);
 }
 /* Mean intensity of UV background at Lyman edge (cgs units)  */
 if (fscanf(param_file, "J_L=%lf\n", &J_L) != 1) {
  printf("Error reading J_L. n_H=%e,sizeZ=%d, num_quadr=%d,Temp=%f\n", nTot[1],
         sizeZ, num_quadr, Temp);
  exit(1);
 }

 /* Limit number of timesteps */
 if (fscanf(param_file, "maxtsteps= %d\n", &maxtsteps) != 1) {
  printf("Error reading maxtsteps. n_H=%lf\n", nTot[1]);
  exit(1);
 }
 if (fscanf(param_file, "numdumps= %d\n", &numdumps) != 1) {
  printf("Error reading numdumps. n_H=%lf\n", nTot[1]);
  exit(1);
 }
 if (numdumps <= 0) {
  printf("invalid numdumps = %d\n", numdumps);
  exit(1);
 }
 double dumptimes[numdumps];
 for (i = 0; i < numdumps; i++) {
  int nRead = fscanf(param_file, "%le", &dumptimes[i]);
  if (nRead == EOF) {
   printf("ERROR: %s: %i: Unable to read dumptimes\n", __FILE__, __LINE__);
   exit(-1);
  }
  printf("%e\t\t", dumptimes[i]);
 }
 char dumpfilenames[numdumps][70];
 for (i = 0; i < numdumps; i++) {
  int nRead = fscanf(param_file, "%s", dumpfilenames[i]);
  if (nRead == EOF) {
   printf("ERROR: %s: %i: Unable to read dumpfilenames\n", __FILE__, __LINE__);
   exit(-1);
  }
  printf("%s\t\t", dumpfilenames[i]);
 }

 printf("\nmetallicities=\t");  /*Read in the metallicities */
 for (i = 1; i < sizeZ; ++i) {
  if (fscanf(param_file, "%lf", &metallis[i]) != 1) {
   printf("Error reading metallicity (i=%d)\n", i);
   exit(1);
  }
  printf("%.3e\t\t", metallis[i]);
 }
 if (metallis[1] != 1.0) {
  printf("\nmetallis[1]!=1.0\t (Floating precision error?)\nExiting.\n");
  exit(1);
 }

 if (sizeZ == 31) {             /* If using everything (all species of all elements), list them */
  printf("\nZ=\t");
  for (i = 1; i < sizeZ; ++i) {
   Z[i] = i;
   printf("%d\t\t", Z[i]);
   if (Z[i] > 30)
    exit(1);
  }

  printf("\nJ=\t");
  for (i = 1; i < sizeZ; ++i) {
   J[i] = i + 1;
   printf("%d\t\t", J[i]);
   if (J[i] > 31)
    exit(1);
  }
 } else {                       /*If not using all species of all elements, read in the custom values */
  printf("\nZ=\t");
  for (i = 1; i < sizeZ; ++i) {
   int nRead = fscanf(param_file, "%d", &Z[i]);
   if (nRead == EOF) {
    printf("ERROR: %s: %i: Unable to read Z metalicity\n", __FILE__, __LINE__);
    exit(-1);
   }
   printf("%d\t\t", Z[i]);
   if (Z[i] > 30)
    exit(1);
  }

  printf("\nJ=\t");
  for (i = 1; i < sizeZ; ++i) {
   int nRead = fscanf(param_file, "%d", &J[i]);
   if (nRead == EOF) {
    printf("ERROR: %s: %i: Unable to read J metalicity\n", __FILE__, __LINE__);
    exit(-1);
   }
   printf("%d\t\t", J[i]);
   if (J[i] > 31)
    exit(1);
  }
 }

 //Here use read-in nTot[1].
 printf("\nnTot=\t%.3e\t", nTot[1]);
 for (i = 2; i < sizeZ; ++i) {  /*calc nTot for He up. */
  nTot[i] = metallis[i] * nTot[1];      /* nTot in cm^-3 */
  printf("%.3e\t", nTot[i]);
 }

 printf("\n\n");

 fclose(param_file);

 double **f;
 double **fdot;
 double **c_H;
 double **c_He;
 double **r_H;
 double **r_He;
 double **r;
 double **c;
 double **d;
 double **a;
 double **p;
 double ***q;
 double ****photoprob;

 double n_e;                    /* number density for electrons */

 int maxelecs;

 f = memarray2(f, sizeZ, &J[0]);
 fdot = memarray2(fdot, sizeZ, &J[0]);
 c_H = memarray2(c_H, sizeZ, &J[0]);
 c_He = memarray2(c_He, sizeZ, &J[0]);
 r_H = memarray2(r_H, sizeZ, &J[0]);
 r_He = memarray2(r_He, sizeZ, &J[0]);
 r = memarray2(r, sizeZ, &J[0]);
 c = memarray2(c, sizeZ, &J[0]);
 d = memarray2(d, sizeZ, &J[0]);
 a = memarray2(a, sizeZ, &J[0]);
 p = memarray2(p, sizeZ, &J[0]);
    /****************/


 q = memarray3(q, sizeZ, &J[0], 1);

 maxelecs = Z[sizeZ - 1];       /*Atomic # of highest element. */
 photoprob = memarray4(photoprob, sizeZ, &J[0], 0, 7, maxelecs);


#ifdef VERBOSE
 printf("Assigned memory. still ok\n"); /*test */
#endif

/*----------------------------------------------------*/
/****************/


/* Zero the arrays */
 for (i = 1; i < sizeZ; ++i) {  /* Big arrays were assigned with a null zeroth line (i=0) */
  for (j = 0; j <= J[i]; ++j) {
   c_H[i][j] = 0.0;
   c_He[i][j] = 0.0;
   r_H[i][j] = 0.0;
   r_He[i][j] = 0.0;
   r[i][j] = 0.0;
   c[i][j] = 0.0;
   d[i][j] = 0.0;
   a[i][j] = 0.0;
   p[i][j] = 0.0;
  }
 }


    /*----*/

 /* initial values for f and fdot */
 for (i = 1; i < sizeZ; ++i) {
  f[i][0] = 0.0;                /* dummy value: 0 index not used here */
  fdot[i][0] = 0.0;             /* dummy value: 0 index not used here */
  f[i][1] = 1.0;                /* All elements start neutral */
  fdot[i][1] = 0.0;
  for (j = 2; j <= J[i]; ++j) {
   f[i][j] = 0.0;
   fdot[i][j] = 0.0;
  }
 }

/* CALCULATE ALL THE RATE COEFFICIENTS! */
 calc_rates(c_H,
            c_He,
            r_H,
            r_He,
            r,
            c,
            d,
            a,
            p,
            q,
            photoprob,
            sizeZ,
            Z, J, maxelecs, num_quadr, J_L, Temp, dummy_d, dummy_ptd, dummy_d);

/****************/
/*---------------------------------------------------- */

#ifdef VERBOSE
 /* Open file and write all coefficients */
 FILE *allcoeffs;
 if ((allcoeffs = fopen("coeffs_out.dat", "w")) == NULL) {
  printf("Can't open output file.. exiting to system\n");
  exit(1);
 }

 for (i = 1; i < sizeZ; ++i) {
  for (j = 1; j <= J[i]; ++j) {
   fprintf(allcoeffs,
           "%d\t%d\tc_H = %e\tc_He = %e\tc = %e\na = %e\tp = %e\tr = %e\td = %e\nr_H = %e\tr_He = %e\n\n",
           i, j, c_H[i][j], c_He[i][j], c[i][j], a[i][j], p[i][j], r[i][j],
           d[i][j], r_H[i][j], r_He[i][j]);
  }
  fprintf(allcoeffs,
          "------------------------------------------------------------------\n");
 }

 if (fclose(allcoeffs) != 0) {
  printf("Can't close coeffs file.. exiting to system\n");
  exit(1);
 }
#endif
    /*----------------------------------------------------*/

#ifdef VERBOSE
 /* Open output file and write first line for initial fractions */
 if ((outfile = fopen("fracs_outer.dat", "w")) == NULL) {
  printf("Can't open output file.. exiting to system\n");
  exit(1);
 }
 t = t_start;                   /* t is declared = t_start above */
 fprintf(outfile, "%e\n", t);
 for (i = 1; i < sizeZ; ++i) {
  for (j = 1; j <= J[i]; ++j) {
   fprintf(outfile, "\t%e", f[i][j]);
  }
 }
 fprintf(outfile, "\n");
#endif
    /*----------------------------------------------------*/

 // printf("%e\t%e\t%e\t%e\n\n",f[1][1],f[2][1],f[3][1],f[4][1]);

 //    printf("\nstart of time loop\n");

 t = timeloop(f,
              fdot,
              c_H,
              c_He,
              r_H,
              r_He,
              r,
              c,
              d,
              a,
              p,
              q,
              photoprob,
              &n_e,
              t_start,
              sizeZ,
              Z,
              nTot,
              metallis,
              J,
              maxelecs,
              num_quadr,
              Temp,
              timescale,
              outfile, maxtsteps, dumptimes, dumpfilenames, numdumps);

 //    printf("end of time loop, t = %e\n",t);   /*test*/

 checkFractions(f, sizeZ, J, __FILE__, __LINE__);

 if ((outfile2 = fopen("final_fracs.dat", "w")) == NULL) {
  printf("Can't open output file 2.. exiting to system\n");
  exit(1);
 }
 for (i = 1; i < sizeZ; ++i) {
  for (j = 1; j <= J[i]; ++j) {
   /* Format to copy Cloudy's output. */
   fprintf(outfile2, "%.3f ", DMAX(-30.0, log10(f[i][j])));
  }
  fprintf(outfile2, "\n");
 }
 if (fclose(outfile2) != 0) {
  printf("Can't close output file 2.. exiting to system\n");
  exit(1);
 }
#ifdef VERBOSE
 if (fclose(outfile) != 0) {
  printf("Can't close output file.. exiting to system\n");
  exit(1);
 }
#endif

    /*** Output ***/
 printf("\nOutput:\nTime at end of run = %e s\n\n", t);



    /****free dynamically allocated memory****/

 for (i = 1; i < sizeZ; ++i) {
  freemem(f[i]);
  freemem(fdot[i]);
  freemem(c_H[i]);
  freemem(c_He[i]);
  freemem(r_H[i]);
  freemem(r_He[i]);
  freemem(r[i]);
  freemem(c[i]);
  freemem(d[i]);
  freemem(a[i]);
  freemem(p[i]);
 }
 freemem2(f);
 freemem2(fdot);
 freemem2(c_H);
 freemem2(c_He);
 freemem2(r_H);
 freemem2(r_He);
 freemem2(r);
 freemem2(c);
 freemem2(d);
 freemem2(a);
 freemem2(p);

    /**  Free dynamically assigned memory for q[][][]  **/
 for (i = 2; i < sizeZ; ++i) {
  for (j = 1; j <= J[i]; ++j) {
   freemem(q[i][j]);
  }
  freemem2(q[i]);
 }
 freemem3(q);

    /****end of freeing dynamically allocated memory****/


 printf("Done.\n");             /*test */
 return (0);
}

/*---------------------------------------------------------*/

/* memarray2: Dynamically assigns memory for 2D arrays */
double **memarray2(
 double **arraynm,
 int sizeZ,
 int *J)
{
 int i;
 arraynm = calloc(sizeZ, sizeof(double *));
 arraynm[0] = NULL;
 for (i = 1; i < sizeZ; ++i) {
  arraynm[i] = calloc((J[i] + 1), sizeof(double));
  arraynm[i][0] = 0.0;          /* new */
 }

 return arraynm;
}

/*---------------------------------------------------------*/

/* memarray3: Dynamically assigns memory for 3D arrays */
double ***memarray3(
 double ***arraynm,
 int sizeZ,
 int *J,
 int maxnull)
{
 int i, j;
 arraynm = calloc(sizeZ, sizeof(double **));
 for (i = 0; i < sizeZ; ++i) {
  if (i <= maxnull) {
   arraynm[i] = NULL;
  } else {
   arraynm[i] = calloc((J[i] + 1), sizeof(double *));
   arraynm[i][0] = NULL;
   for (j = 1; j <= J[i]; ++j) {
    arraynm[i][j] = calloc((J[i] + 1), sizeof(double));
    arraynm[i][j][0] = 0.0;     /* new */
   }
  }
 }

 return arraynm;
}

/*---------------------------------------------------------*/

/* memarray4: Dynamically assigns memory for 4D arrays */
double ****memarray4(
 double ****arraynm,
 int sizeZ,
 int *J,
 int maxnull,
 int kmax,
 int lmax)
{
 int i, j, k;
 arraynm = calloc(sizeZ, sizeof(double ***));
 for (i = 0; i < sizeZ; ++i) {
  if (i <= maxnull) {
   arraynm[i] = NULL;
  } else {
   arraynm[i] = calloc((J[i] + 1), sizeof(double **));
   arraynm[i][0] = NULL;
   for (j = 1; j <= J[i]; ++j) {
    arraynm[i][j] = calloc((kmax + 1), sizeof(double *));
    arraynm[i][j][0] = NULL;    /* new */
    for (k = 1; k <= kmax; ++k) {
     arraynm[i][j][k] = calloc((lmax + 1), sizeof(double));
     arraynm[i][j][k][0] = 0.0; /* new */
    }
   }
  }
 }

 return arraynm;
}

/*---------------------------------------------------------*/

/* freemem: Frees dynamically assigned memory for array of doubles */
void freemem(
 double *arraynm)
{
 if (arraynm == NULL) {
  printf("freemem:  Null array detected.\n");
 } else {
  free(arraynm);
 }
}

/*---------------------------------------------------------*/

/* freemem2: Frees dynamically assigned memory for array of pointers */
void freemem2(
 double **arraynm)
{
 if (arraynm == NULL) {
  printf("freemem2: Null array detected.\n");
 } else {
  free(arraynm);
 }
}

/*---------------------------------------------------------*/

/* freemem3: Frees dynamically assigned memory for array of pointers */
void freemem3(
 double ***arraynm)
{
 if (arraynm == NULL) {
  printf("freemem3: Null array detected.\n");
 } else {
  free(arraynm);
 }
}

/*-------------------------------------------------------------*/
