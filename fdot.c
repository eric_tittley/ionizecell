/* Computes total rates for hydrogen, helium and metals
 * -------------------------------------------------------------------------
 * -------------------------------------------------------------------------
 * Authors:             Avery Meiksin    (IfA, U Edinburgh)
 *                      Simon Reynolds   (IfA, U Edinburgh, for PhD)
 * Written:             2007-2009        (as part of timedep_* series)
 * Modified:            10-Jul-2013      (cleaned up by AM)
 *
 * -------------------------------------------------------------------------
 * Called by timeloop in Euler_loop.c.
 */
#include "IonizeCell.h"

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
//stdlib only needed if using exit() 

#include "NumericalTolerances.h"

double timecheck(
 double timescale,
 int const i,
 int const j,
 double **f,
 double **fdot);

double calcfdot(
 double **f,
 double **fdot,
 double **c_H,
 double **c_He,
 double **r_H,
 double **r_He,
 double **r,
 double **c,
 double **d,
 double **a,
 double **p,
 double ***q,
 double *n_e,
 int sizeZ,
 double *nTot,
 int *J,
 double timescale)
{
 int i, j, k, l;
 double nHHe[3][3];             /* number densities for HI,HII,HeI,HeII (not HeIII) */
 double n_ij;                   /* number density for ion ij */

 /* Values of summed terms in rate eqns */
 double nc_H, nc_He, nr_H, nr_He;

 double q_ijk, fq_ilj;

 /* Initialise summed rates */
 nc_H = 0.0;
 nc_He = 0.0;
 nr_H = 0.0;
 nr_He = 0.0;

      /*******   ELECTRONS   *********/
 for (i = 1; i <= 2; ++i) {
  for (j = 1; j <= 2; ++j) {
   nHHe[i][j] = f[i][j] * nTot[i];
   if (nHHe[i][j] < 0.0)
    printf("nHHe[%d][%d] = %e\n", i, j, nHHe[i][j]);
  }
 }

      /*******    Eqn.(5)   *********/
 *n_e = nHHe[1][2] + nHHe[2][2] + 2.0 * f[2][3] * nTot[2];      /* n_12+n_22+ 2(n_23) */
      /*******   H & He   *********/
 for (i = 1; i < sizeZ; ++i) {
  for (j = 1; j <= J[i]; ++j) {
   n_ij = f[i][j] * nTot[i];

   if (j <= (J[i] - 1)) {

    /*elements other than H */
    if (i >= 2)
     nc_H += n_ij * c_H[i][j];
    //printf("%d,%d\tn=%e=%e*%e\tc_H=%e\tnc_H=%e\n",i,j,n_ij,f[i][j],nTot[i],c_H[i][j],nc_H);

    /*elements other than He */
    if (i == 1 || i >= 3)
     nc_He += n_ij * c_He[i][j];
   }

   if (j >= 2) {
    if (i >= 2)
     nr_H += n_ij * r_H[i][j];
    if (i == 1 || i >= 3)
     nr_He += n_ij * r_He[i][j];
   }
  }
 }                              /*end of charge-transfer sums */

      /*******   Eqn.(6)   *********/
 fdot[1][1] = f[1][2] * (*n_e * r[1][1] + nc_H)
  - f[1][1] * (p[1][1] + *n_e * c[1][1] + nr_H);
#ifdef VERBOSE
 printf("%e, %e, %e, %e, %e, %e, %e, %e,  \n", f[1][2], *n_e, r[1][1], nc_H,
        f[1][1], p[1][1], c[1][1], nr_H);
#endif
 fdot[1][2] = -fdot[1][1];

 fdot[2][1] = f[2][2] * (*n_e * (r[2][1] + d[2][1]) + nc_He)
  - f[2][1] * (*n_e * (c[2][1] + a[2][1]) + p[2][1]
               + nr_He + q[2][1][3]);   /* ijk is 213 */

 fdot[2][3] = f[2][2] * (*n_e * (c[2][2] + a[2][2]) + nHHe[1][2] * c_H[2][2]
                         + p[2][2])
  + f[2][1] * q[2][1][3]        /* Verner's equation (4) says q[2][3][1] ... */
  /*  but this q must be the same process as in q[2][1][3] above... */
  -f[2][3] * (*n_e * (r[2][2] + d[2][2]) + nHHe[1][1] * r_H[2][2]);

 fdot[2][2] = -fdot[2][1] - fdot[2][3];

#ifdef VERBOSE
 printf("fdot  %e\n", fdot[1][1]);
#endif
 if (fdot[1][1] == 0.0) {
  timescale = VERY_LARGE_TIME;
 } else {
  timescale = fabs(f[1][1] / fdot[1][1]);
  if (!isfinite(timescale)) {
   printf("%s: %i: ERROR: timescale=%f; f=%5.3e; fdot=%5.3e\n",
          __FILE__, __LINE__, timescale, f[1][1], fdot[1][1]);
   exit(EXIT_FAILURE);
  }
 }
#ifdef VERBOSE
 printf("After [1][1], timescale = %e\n", timescale);
#endif
 timescale = timecheck(timescale, 2, 1, f, fdot);
#ifdef VERBOSE
 printf("After [2][1], timescale = %e\n", timescale);
#endif
 timescale = timecheck(timescale, 2, 2, f, fdot);
#ifdef VERBOSE
 printf("After [2][2], timescale = %e\n", timescale);
#endif
 timescale = timecheck(timescale, 2, 3, f, fdot);
#ifdef VERBOSE
 printf("After [2][3], timescale = %e\n", timescale);
#endif

      /*******   Metals   *********/
 for (i = 3; i < sizeZ; ++i) {
  for (j = 1; j <= J[i]; ++j) {
   for (k = j + 2; k <= J[i]; ++k) {
    q_ijk += q[i][j][k];
   }
   for (l = 1; l <= (j - 2); ++l) {
    fq_ilj += f[i][l] * q[i][l][j];     /* Verner's equation (4) says ijl */
   }

          /*******   Eqn.(4)   *********/
   fdot[i][j] =
    (f[i][j + 1] * (*n_e * (r[i][j] + d[i][j]) + nHHe[1][1] * r_H[i][j]
                    + nHHe[2][1] * r_He[i][j])
     + f[i][j - 1] * (*n_e * (c[i][j - 1] + a[i][j - 1]) +
                      nHHe[1][2] * c_H[i][j - 1]
                      + nHHe[2][2] * c_He[i][j - 1] + p[i][j - 1])
     + fq_ilj
     - f[i][j] * (*n_e * (c[i][j] + a[i][j] + r[i][j - 1] + d[i][j - 1])
                  + nHHe[1][2] * c_H[i][j] + nHHe[2][2] * c_He[i][j]
                  + nHHe[1][1] * r_H[i][j - 1] + nHHe[2][1] * r_He[i][j - 1]
                  + p[i][j] + q_ijk)
    );

   timescale = timecheck(timescale, i, j, f, fdot);
#ifdef VERBOSE
   printf("After [%d][%d], timescale = %e\n", i, j, timescale);
#endif
   q_ijk = 0.0;
   fq_ilj = 0.0;
  }

 }
      /*******   (End of metals)   *********/

 checkFractions(f, sizeZ, J, __FILE__, __LINE__);

 return timescale;
}

/*----------------------------------------------------------*/
/* timecheck: Updates timescale if the new value is smaller.*/
/*   timescale is zero until a non-zero value is available. */
/*   timescale is passed in by value, and then returned.    */
double timecheck(
 double timescale,
 int const i,
 int const j,
 double **const f,
 double **const fdot)
{
 if (fabs(fdot[i][j]) < VERY_SMALL_NUMBER)      /* check if zero */
  return timescale;
 if (f[i][j] < 1e-4)
  return timescale;             //"floor"

 double newtime;
 newtime = fabs(f[i][j] / fdot[i][j]);
 if (!isfinite(newtime)) {
  printf("%s: %i: ERROR: newtime=%f; f[%i][%i]=%5.3e; fdot=%5.3e\n",
         __FILE__, __LINE__, newtime, i, j, f[i][j], fdot[i][j]);
  exit(EXIT_FAILURE);
 }

 if (fabs(newtime) < VERY_SMALL_NUMBER) {
  return timescale;
 } else {
  //printf("Non-zero timescale detected!\n");
  if (fabs(timescale) < VERY_SMALL_NUMBER) {
   return newtime;
  } else {
   /* neither are zero */
   timescale = (timescale < newtime ? timescale : newtime);
  }
 }

 return timescale;
}

/*---------------------------------------------------------*/
