#include "IonizeCell.h"

#include <math.h>
#include <stdio.h>

void checkFractions(
 double **const f,
 const int sizeZ,
 int const *const J,
 const char *FILE,
 const int LINE)
{

 for (size_t i = 1; i < sizeZ; ++i) {   /* Big arrays were assigned with a null zeroth line (i=0) */
  for (size_t j = 0; j <= J[i]; ++j) {
   if (!isfinite(f[i][j])) {
    printf("%s: %i: ERROR f[%lu][%lu]=%f\n", FILE, LINE, i, j, f[i][j]);
   }
   if (f[i][j] < 0.0) {
    printf("%s: %i: ERROR f[%lu][%lu]=%5.3e\n", FILE, LINE, i, j, f[i][j]);
   }
  }
 }

}
