IonizeCell evolves the time-dependent ionization equations for
hydrogen, helium and a large number of metals for a fixed cell of
given hydrogen density, gas temperature, and chemical abundances
provided by the user.

The code is based on a series of codes written by Avery Meiksin and
Simon Reynolds (IfA, U Edinburgh). The development and results for a
variety of applications to cosmological simulations are published in
the PhD thesis "Time-dependent Ionisation of Metals in the
Intergalactic Medium" (2009, University of Edinburgh) by Simon
Reynolds. Publications using the code should make reference to the
thesis.

TO USE:

The code is compiled through 'make all.' For very verbose output, use
'make all_verbose.' It is executed on an input file, which must contain:

sizeZ= (int)      !number of species + 1, including hydrogen and helium
num_quadr= (int)  !number of quadrature nodes (eg 25) for integrating p.i. rates
Temp= (float)     !gas temperature in K
n_H= (float)      !hydrogen number density in cm^-3
J_L= (float)      !UV intensity at Lyman edge (erg/cm^2/s/Hz/sr)
maxtsteps= (int)  !maximum number of time steps to take
numdumps= (int)   !number of time dump files for fractions
(numdumps dump times in secs)
t1
t2
...
tn
(numdumps filenames)
file_t1.out
file_t2.out
...
file_tn.out
(number abundances of elements scale to hydrogen, up to sizeZ-1 in number)
1.00E+00  !hydrogen
a2
...
(atomic numbers of the elements, up to sizeZ-1 in number)
1         !hydrogen
AN2
...
(maximum ionization states of the elements to consider)
2         !hydrogen
JMAX2
...

A final integration result for the ionization fractions f_X is
output in final_fracs.dat. The format style is:

f_HI f_HII
f_HeI f_HeII f_HeIII
etc.

CONTACT:

Questions may be addressed to Avery Meiksin (A.Meiksin@ed.ac.uk).
