/* Read in rate-coefficients data, and store in arrays
 * -------------------------------------------------------------------------
 * -------------------------------------------------------------------------
 * Authors:             Avery Meiksin    (IfA, U Edinburgh)
 *                      Simon Reynolds   (IfA, U Edinburgh, for PhD)
 * Written:             2007-2009        (as part of timedep_* series)
 * Modified:            10-Jul-2013      (cleaned up by AM)
 *
 * -------------------------------------------------------------------------
 * Called by IonizeCell.c.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "AtomicDataLib/autoion.h"
void nrerror(
 char error_text[]);
float *vector(
 long nl,
 long nh);
void free_vector(
 float *v,
 long nl,
 long nh);


extern void phfit2_(
 int *,
 int *,
 int *,
 double *,
 double *);
extern void myeion_(
 int *,
 int *,
 int *,
 float *);
extern float hction_(
 int *,
 int *,
 float *);
extern float hctrecom_(
 int *,
 int *,
 float *);
extern void cfit_(
 int *,
 int *,
 float *,
 double *);

void gauleg(
 float x1,
 float x2,
 float x[],
 float w[],
 int n);
void ChrTrfData(
 char *filenm,
 int numlines,
 int fittype,
 double **datarray,
 int sizeZ,
 int *Z,
 int *J,
 float Temp);
void radR_VF(
 char *filenm,
 int numlines,
 double **datarray,
 int sizeZ,
 int *Z,
 int *J,
 float Temp);
void radR_pl(
 char *filenm,
 int numlines,
 double **datarray,
 int sizeZ,
 int *Z,
 int *J,
 float Temp);
void diR_low(
 char *filenm,
 int numlines,
 double **datarray,
 int sizeZ,
 int *Z,
 int *J,
 float Temp);
void diR_kludge(
 double **datarray,
 int sizeZ,
 int *Z,
 int *J);
double ChrTrfFit1(
 float a,
 float b,
 float c,
 float d,
 float T_4);
double ChrTrfFit2(
 float a,
 float b,
 float c,
 float d,
 float T_4);

float photofunc(
 float E_erg,
 int Z,
 int numelecs,
 int shell,
 double *dummyarray,
 double redshift);

static const float planck = 6.6260688e-27; /* cgs units */ ;

static const float E_thresh[3] = { 2.18e-11, 3.94e-11, 8.72e-11 };

       /* These are 13.6eV, 24.6eV, 54.4eV (Ost.Pg23) in ergs */

void calc_rates(
 double **c_H,
 double **c_He,
 double **r_H,
 double **r_He,
 double **r,
 double **c,
 double **d,
 double **a,
 double **p,
 double ***q,
 double ****photoprob,
 int sizeZ,
 int *Z,
 int *J,
 int maxelecs,
 int num_quadr,
 double J_L,
 float Temp,
 double R_0,
 double *ColDen,
 double redshift)
{
 int i, j, k, l, x;
 const char *AtomicDataDir = getenv("ATOMIC_DATA_DIR");
 if (AtomicDataDir == NULL) {
  printf
   ("%s: %i: Please set the enviroment variable ATOMIC_DATA_DIR to point to the path to the Atomic Data.\n",
    __FILE__, __LINE__);
  exit(-1);
 }
 char ChrTrfFileNmRecHe[MAX_PATH_LENGTH];
 char ChrTrfFileNmIonHe[MAX_PATH_LENGTH];
 char radRFileNmVF[MAX_PATH_LENGTH];
 char radRFileNmpl[MAX_PATH_LENGTH];
 char diRFileNmlow[MAX_PATH_LENGTH];
 snprintf(ChrTrfFileNmRecHe, sizeof(ChrTrfFileNmRecHe), "%s/%s", AtomicDataDir,
          "ChrgTrfRecmHe.dat");
 snprintf(ChrTrfFileNmIonHe, sizeof(ChrTrfFileNmIonHe), "%s/%s", AtomicDataDir,
          "ChrgTrfIonnHe.dat");
 /* NB Removed one line in radR_VF95.dat: HeI high temp */
 snprintf(radRFileNmVF, sizeof(radRFileNmVF), "%s/%s", AtomicDataDir,
          "radR_VF95.dat");
 snprintf(radRFileNmpl, sizeof(radRFileNmpl), "%s/%s", AtomicDataDir,
          "radR_pl.dat");
 snprintf(diRFileNmlow, sizeof(diRFileNmlow), "%s/%s", AtomicDataDir,
          "diR_low.dat");
 FILE *augerfile;

 int augerdata[1091][17];
 float dummy_fl;

 int wts_ok;
 float abscis[num_quadr];
 float weights[num_quadr];
 float E_ion;                   /* E_ij from eqn 11 */
 double photoint;
 double photorate;              /* rate of photoionisation J_ijnl in eqn 11 */
 int numelecs;
 int shell;                     /* First thirty elements have seven shells nl labelled:
                                 * 1 - 1s, 
                                 * 2 - 2s, 3 - 2p, 
                                 * 4 - 3s, 5 - 3p, 6 - 3d, 
                                 * 7 - 4s. */

 for (i = 1; i < sizeZ; ++i) {
  k = Z[i];
  for (j = 1; j < J[i]; ++j) {  /* No rates for ion'n-from or recomb-to fully ionised J[i] */

   l = Z[i] - j + 1;            /*numelecs */
   a[i][j] = autoion(k, l, Temp);

   /* cfit_ has data for Z=1-28 for all species of except fully-ionized */
   if (k <= 28 && j < J[i]) {
    cfit_(&k, &l, &Temp, &c[i][j]);     /* Kelvin. It gets multiplied by k_B(eV/K) */
/*     	    printf("%d\t%d\t%e\n",i,j,c[i][j]); */
   }

        /*** my_ct.f ***/
   /* hction_ has data for Z=3-30,j=1-3 */
   if (k >= 3 && k <= 30 && j <= 3) {
    l = j;
#ifdef VERBOSE
    printf("c_H[%d][%d] = %e then ", i, j, c_H[i][j]);
#endif
    c_H[i][j] = (double) hction_(&l, &k, &Temp);        /* Kelvin. It gets divided by 10^4K */
   }

   /* hctrecom_ has data for Z=2,j=1-2; Z=3,j=1-3; Z=4-30,j=1-4 */
   /* It has a statistical equation for j=5-(J-1) */
   if (k >= 2 && k <= 30 && j < J[i]) {
    l = j + 1;
    r_H[i][j] = (double) hctrecom_(&l, &k, &Temp);
   }

   if (c_H[i][j] * 0.0 != 0.0)
    c_H[i][j] = 0.0;            /* Zero c_H when NaN */
#ifdef VERBOSE
   printf(" %e\tc_H[%d][%d]\n", c_H[i][j], i, j);
#endif
   if (r_H[i][j] * 0.0 != 0.0)
    r_H[i][j] = 0.0;            /* Zero r_H when NaN */

  }
 }
#ifdef VERBOSE
 if (sizeZ >= 4) {
  printf("CI  -> CII : c_H[3][1] = %e\n", c_H[3][1]);
  printf("CII -> CI  : r_H[3][1] = %e\n", r_H[3][1]);
 }
#endif

 /* Call functions to read in datafiles and fill arrays */
 ChrTrfData(ChrTrfFileNmRecHe, 21, 1, r_He, sizeZ, &Z[0], &J[0], Temp);
 ChrTrfData(ChrTrfFileNmIonHe, 7, 2, c_He, sizeZ, &Z[0], &J[0], Temp);
 radR_pl(radRFileNmpl, 167, r, sizeZ, &Z[0], &J[0], Temp);      /* power-laws */
 /* The VF radiative recombination data is more accurate. For the available
  * species it over-writes the power-law values in r[][] */
 /*NB Removed line3 from radRFileNmVF datafile, HeI high temp. Kept low-temp value. */
 radR_VF(radRFileNmVF, 107, r, sizeZ, &Z[0], &J[0], Temp);
 diR_low(diRFileNmlow, 25, d, sizeZ, &Z[0], &J[0], Temp);
 diR_kludge(d, sizeZ, &Z[0], &J[0]);    /* This is CLOUDY's kludge */

 /* Prepare to read auger electron probability data */
 for (i = 1; i < sizeZ; ++i) {
  for (j = 1; j <= J[i]; ++j) {
   for (shell = 1; shell <= 7; ++shell) {
    for (l = 1; l <= maxelecs; ++l) {
     if (l == 1) {
      photoprob[i][j][shell][l] = 1.0;  /*Default case is only emit one electron   */
     } else {                   /*ie. no Auger elecs; Probability(l=1) = 1 */
      photoprob[i][j][shell][l] = 0.0;
     }

    }
   }
  }
 }

 char AugerTableFile[MAX_PATH_LENGTH];
 snprintf(AugerTableFile, sizeof(AugerTableFile), "%s/%s", AtomicDataDir,
          "auger_table.dat");
 if ((augerfile = fopen(AugerTableFile, "r")) == NULL) {
  printf("ERROR: %s: %i: Can't open file %s\n", __FILE__, __LINE__,
         AugerTableFile);
  exit(-1);
 }

 for (x = 1; x <= 1090; ++x) {
  if (fscanf(augerfile, "%d%d%d%f%f%d%d%d%d%d%d%d%d%d%d%d",
             &augerdata[x][1],
             &augerdata[x][2],
             &augerdata[x][3],
             &dummy_fl,
             &dummy_fl,
             &augerdata[x][6],
             &augerdata[x][7],
             &augerdata[x][8],
             &augerdata[x][9],
             &augerdata[x][10],
             &augerdata[x][11],
             &augerdata[x][12],
             &augerdata[x][13],
             &augerdata[x][14], &augerdata[x][15], &augerdata[x][16]) != 16) {
   printf("Error reading augerfile.. exiting to system\n");
   exit(1);
  }
  /*printf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d\n",augerdata[x][1],augerdata[x][2],augerdata[x][3],augerdata[x][6],augerdata[x][7],augerdata[x][8],augerdata[x][9],augerdata[x][10],augerdata[x][11],augerdata[x][12],augerdata[x][13],augerdata[x][14],augerdata[x][15],augerdata[x][16]); */
 }

 if (fclose(augerfile) != 0) {
  printf("Can't close file.. exiting to system\n");
  exit(1);
 }

 /* for each line x of augerdata, if it's an element we're using... */
 for (x = 1; x <= 1090; ++x) {
  for (i = 1; i < sizeZ; ++i) {
   if (augerdata[x][1] == Z[i]) {

    /* ...and if it's a species we're using... */
    for (j = 1; j <= J[i]; ++j) {
     if (augerdata[x][2] == j) {

      /* ...write that data into the array photoprob[][][][].  */
      shell = augerdata[x][3];
      for (l = 1; l <= 10; ++l) {       /*probability of emmiting l elecs  */
       if (l <= maxelecs) {     /*Cannot emit more elecs than had! */
        photoprob[i][j][shell][l] = augerdata[x][6 + l] / 10000.0;
       }
      }
      break;                    /* Should lead to second break */

     }
    }
    break;                      /* Should move to next x */

   }
  }
 }

 for (i = 1; i < sizeZ; ++i) {
  for (j = 1; j <= J[i]; ++j) {
   numelecs = Z[i] - j + 1;     /* not using ( J[i]-j ) just in case J[i]!=Z[i]+1  */
   for (shell = 1; shell <= 7; ++shell) {
    E_ion = 0.0;
    myeion_(&Z[i], &numelecs, &shell, &E_ion);
    E_ion *= 1.602e-12;         /* From eV to ergs */
    // printf("E_ion(%d,%d,%d) =\t%e\n",i,j,shell,E_ion); 

/*---------------------------------------------------------*/
    photoint = 0.0;
    /*integration */
    /* pointers-1 because gauleg's arrays start at [1]. */
    gauleg(E_ion, (10.0 * E_ion), abscis - 1, weights - 1, num_quadr);
    wts_ok = 0;
    for (x = 0; x < num_quadr; ++x) {
     if (weights[x] > 0)
      wts_ok = 1;
     break;
    }
    if (wts_ok) {
     for (x = 0; x < num_quadr; ++x) {  /* [1..num_quadr] in gauleg,[0..num_quadr-1] here */
      photoint +=
       weights[x] * photofunc(abscis[x], Z[i], numelecs, shell, &ColDen[0],
                              redshift);
     }
    }
    photorate = 4.0 * M_PI * J_L * photoint / planck;   /* included 1/h (cgs units) */

/*---------------------------------------------------------*/

    p[i][j] += photorate * photoprob[i][j][shell][1];
    //      printf("p[%d][%d] (so far...) = %g\n",i,j,p[i][j]);
    //printf("%d %d %d - Added 4pi * %e * %e * %e * %e\n",i,j,shell,J_L,photoint,photoprob[i][j][shell][1],(1/planck));

    if (i >= 2) {               /* q[][][] not used for hydrogen */
     for (k = 1; k <= J[i]; ++k) {      /* This goes from l=1 -> k=J[i]  */
      /*printf("ijk= %d, %d, %d\n", i, j, k); *//*test */
      q[i][j][k] += photorate * photoprob[i][j][shell][k - j];  /*error if k-j > maxelecs */
     }
    }
   }                            /*shell */


   //              printf("p[%d][%d] = %g\n",i,j,p[i][j]);

   if (i >= 2) {                /* q[][][] not used for hydrogen */
    for (k = 1; k <= J[i]; ++k) {
     //      printf("q[%d][%d][%d]=\t%e\n",i,j,k,q[i][j][k]);
    }
   }
  }                             /*j */
 }                              /*i */


 /*
  * for (i=1;i<sizeZ;++i) { 
  * for (j=1;j<=J[i];++j) {
  * printf("p[%d][%d]= %e\ta[%d][%d]= %e\n",i,j,p[i][j],i,j,a[i][j]);
  * }
  * }
  */
#ifdef VERBOSE
 printf
  ("\nSiII:\nc_H[4][2]=\t%e\nc_He[4][2]=\t%e\nr_H[4][2]=\t%e\nr_He[4][2]=\t%e\nr[4][2]=\t%e\nc[4][2]=\t%e\nd[4][2]=\t%e\na[4][2]=\t%e\np[4][2]=\t%e\n\n",
   c_H[4][2], c_He[4][2], r_H[4][2], r_He[4][2], r[4][2], c[4][2], d[4][2],
   a[4][2], p[4][2]);
#endif
}

/*---------------------------------------------------------*/

/* gauleg: NR routine for Gauss-Legendre quadrature. Returns
   n abscissas, x[], and the corresponding n weights, w[]. 
   The integral wrt x, between x1 and x2, of some f is the 
   sum over 1<=i<=n of w[i].f(x[i]) */
#define EPS 3.0e-11
void gauleg(
 float x1,
 float x2,
 float x[],
 float w[],
 int n)
{
 int m, j, i;
 double z1, z, xm, xl, pp, p3, p2, p1;

 m = (n + 1) / 2;
 xm = 0.5 * (x2 + x1);
 xl = 0.5 * (x2 - x1);
 for (i = 1; i <= m; i++) {
  z = cos(3.141592654 * (i - 0.25) / (n + 0.5));
  do {
   p1 = 1.0;
   p2 = 0.0;
   for (j = 1; j <= n; j++) {
    p3 = p2;
    p2 = p1;
    p1 = ((2.0 * j - 1.0) * z * p2 - (j - 1.0) * p3) / j;
   }
   pp = n * (z * p1 - p2) / (z * z - 1.0);
   z1 = z;
   z = z1 - p1 / pp;
  } while (fabs(z - z1) > EPS);
  x[i] = xm - xl * z;
  x[n + 1 - i] = xm + xl * z;
  w[i] = 2.0 * xl / ((1.0 - z * z) * pp * pp);
  w[n + 1 - i] = w[i];
 }
}

#undef EPS

/*---------------------------------------------------------*/

/* ChrTrfData: Helium charge transfer  */
void ChrTrfData(
 char *filenm,
 int numlines,
 int fittype,
 double **datarray,
 int sizeZ,
 int *Z,
 int *J,
 float Temp)
{
 FILE *ctfile;
 int nl, xi, xelecs;            /*line number, element, # of elecs */
 int i, j;
 double a, b, c, d, Tmin, Tmax;

 double T_4 = Temp * 1.0e-4;

 if ((ctfile = fopen(filenm, "r")) == NULL) {
  printf("Can't open datafile %s. Exiting to system\n", filenm);
  exit(1);
 }

 for (nl = 1; nl <= numlines; ++nl) {
  if (fscanf
      (ctfile, "%d%d%lf%lf%lf%lf%lf%lf", &xi, &xelecs, &a, &b, &c, &d, &Tmin,
       &Tmax) != 8) {
   printf("Error reading datafile %s. Exiting to system\n", filenm);
   exit(1);
  }


  /* for each line nl of data, if it's an element we're using... */
  for (i = 1; i < sizeZ; ++i) {
   /* ...and if it's a species we're using... */
   if (xi == Z[i]) {
    for (j = 1; j <= J[i]; ++j) {
     if (xelecs == (Z[i] - j + 1)) {


      if (Temp < Tmin || Temp > Tmax) {
       printf("\nATOM_RATES Warning: Temp (%f) outside accurate range for %s\n",
              Temp, filenm);
       printf("line %d: %lf - %lf\n\n", nl, Tmin, Tmax);
      }

      /* ...calculate the coefficient and write it into the array.  */
      switch (fittype) {
       case 1:
        datarray[i][j] = ChrTrfFit1(a, b, c, d, T_4);
#ifdef VERBOSE
        printf("r_He[%d][%d] = %lf\n", i, j, datarray[i][j]);
#endif
        break;
       case 2:
        datarray[i][j] = ChrTrfFit2(a, b, c, d, T_4);
#ifdef VERBOSE
        printf("c_He[%d][%d] = %e\n", i, j, datarray[i][j]);
#endif
        break;
       default:
        printf("Fit-type given for datafile %s not recognised. Continuing \n",
               filenm);
        datarray[i][j] = ChrTrfFit1(a, b, c, d, T_4);
      }

      break;                    /* Should lead to second break */

     }
    }                           /* j, species */
    break;                      /* Should move to next nl */

   }
  }                             /* i, element */

 }                              /* x, line of data */

 if (fclose(ctfile) != 0) {
  printf("Can't close datafile %s. Exiting to system\n", filenm);
  exit(1);
 }
}

/*---------------------------------------------------------*/

/* ChrTrfFit1:  */
double ChrTrfFit1(
 float a,
 float b,
 float c,
 float d,
 float T_4)
{
 /* T_4 must be given as temp in 10^4K. 'a' is in 10^-9 cm^3/s */
 /* Rate is returned in  cm^3/s  */

 /*    10**(-9)*a*(T/10**4K)**b*[1+c*exp(d*T/10**4K)]  */
 return (1.0e-9) * a * pow(T_4, b) * (1 + c * exp(d * T_4));
}

/*---------------------------------------------------------*/

/* ChrTrfFit2:  */
double ChrTrfFit2(
 float a,
 float b,
 float c,
 float d,
 float T_4)
{
 /* T_4 must be given as temp in 10^4K. 'a' is in 10^-9 cm^3/s */
 /* 'd' is in eV. Rate is returned in  cm^3/s  */

 /* Boltzmann's Constant 
  * k = 8.617339 � 10^-5 electron-volts/kelvin. */

 /*   10**(-9)*a*(T/10**4K)**b*exp(-c*T/10**4K)*exp(-d[eV]/kT) cm**3/s */
 return (1.0e-9) * a * pow(T_4,
                           b) * exp(-c * T_4) * exp(-d / (8.617339e-5 * T_4 *
                                                          1.0e4));
}

/*---------------------------------------------------------*/

/* radR_VF: Radiative recombination rates for H-like, He-like, Li-like and Na-like ions
            over a broad range of temperature. Verner & Ferland, 1996  */
/*NB Removed line3 from datafile, HeI high temp*/
void radR_VF(
 char *filenm,
 int numlines,
 double **datarray,
 int sizeZ,
 int *Z,
 int *J,
 float Temp)
{
 FILE *radRfile;
 int x, xi, xelecs;             /*line number, element, # of elecs */
 int i, j;
 float a, b, T0, T1;

 if ((radRfile = fopen(filenm, "r")) == NULL) {
  printf("Can't open datafile %s. Exiting to system\n", filenm);
  exit(1);
 }

 for (x = 1; x <= numlines; ++x) {

  /*  The * scraps the 9 characters. */
  if (fscanf(radRfile, "%*9c%d%d%f%f%f%f", &xi, &xelecs, &a, &b, &T0, &T1) != 6) {
   printf("Error reading datafile %s. Exiting to system\n", filenm);
   exit(1);
  }
  /*    printf("%d\t%d\t%d\n",x,xi,xelecs); */

  /* (for each line x of data,) if it's an element we're using... */
  for (i = 1; i < sizeZ; ++i) {
   if (xi == Z[i]) {

    /* ...and if it's a species we're using... */
    for (j = 1; j <= J[i]; ++j) {
     if (xelecs == (Z[i] - j + 1)) {


      /* ...calculate the coefficient and write it into the array.  */
      /* a in cm/s */
      datarray[i][j] =
       a / (sqrt(Temp / T0) * pow((1 + sqrt(Temp / T0)), (1 - b))
            * pow((1 + sqrt(Temp / T1)), (1 + b)));

      break;                    /* Should lead to second break */

     }
    }
    break;                      /* Should move to next x */

   }
  }

 }

 if (fclose(radRfile) != 0) {
  printf("Can't close datafile %s. Exiting to system\n", filenm);
  exit(1);
 }
}

/*---------------------------------------------------------*/

/* radR_pl:Power-law fits to the rates of radiative recombination
           from AP73, SvS82, AR85  */
void radR_pl(
 char *filenm,
 int numlines,
 double **datarray,
 int sizeZ,
 int *Z,
 int *J,
 float Temp)
{
 FILE *radRfile;
 int x, xi, xelecs;             /*line number, element, # of elecs */
 int i, j;
 float a, b;

 double T_4 = Temp * 1.0e-4;

 if ((radRfile = fopen(filenm, "r")) == NULL) {
  printf("Can't open datafile %s. Exiting to system\n", filenm);
  exit(1);
 }

 for (x = 1; x <= numlines; ++x) {

  if (fscanf(radRfile, "%d%d%f%f", &xi, &xelecs, &a, &b) != 4) {
   printf("Error reading datafile %s. Exiting to system\n", filenm);
   exit(1);
  }
  /*    printf("%d\t%d\t%d\n",x,xi,xelecs); */

  /* for each line x of data, if it's an element we're using... */
  for (i = 1; i < sizeZ; ++i) {
   if (xi == Z[i]) {

    /* ...and if it's a species we're using... */
    for (j = 1; j <= J[i]; ++j) {
     if (xelecs == (Z[i] - j + 1)) {


      /* ...calculate the coefficient and write it into the array.  */
      datarray[i][j] = a * pow(T_4, -b);        /* a in cm/s */

      break;                    /* Should lead to second break */

     }
    }
    break;                      /* Should move to next x */

   }
  }

 }

 if (fclose(radRfile) != 0) {
  printf("Can't close datafile %s. Exiting to system\n", filenm);
  exit(1);
 }
}

/*---------------------------------------------------------*/

/* diR_low:   */
void diR_low(
 char *filenm,
 int numlines,
 double **datarray,
 int sizeZ,
 int *Z,
 int *J,
 float Temp)
{
 FILE *diRfile;
 int x, xi, xelecs;             /*line number, element, # of elecs */
 int i, j;
 float a, b, c, d, f;

 double T_4 = Temp * 1.0e-4;

 if ((diRfile = fopen(filenm, "r")) == NULL) {
  printf("Can't open datafile %s. Exiting to system\n", filenm);
  exit(1);
 }

 for (x = 1; x <= numlines; ++x) {

  if (fscanf(diRfile, "%d%d%f%f%f%f%f", &xi, &xelecs, &a, &b, &c, &d, &f) != 7) {
   printf("Error reading datafile %s. Exiting to system\n", filenm);
   exit(1);
  }
  /*    printf("%d\t%d\t%d\n",x,xi,xelecs); */

  /* for each line x of data, if it's an element we're using... */
  for (i = 1; i < sizeZ; ++i) {
   if (xi == Z[i]) {

    /* ...and if it's a species we're using... */
    for (j = 1; j <= J[i]; ++j) {
     if (xelecs == (Z[i] - j + 1)) {


      /* ...calculate the coefficient and write it into the array.  */
      /*  10**(-12)*(a/t+b+c*t+d*t**2)*t**(-3/2)*exp(-f/t)  */
      datarray[i][j] =
       1e-12 * (a / T_4 + b + c * T_4 + d * T_4 * T_4) * pow(T_4,
                                                             (-3 / 2)) *
       exp(-f / T_4);

      break;                    /* Should lead to second break */

     }
    }
    break;                      /* Should move to next x */

   }
  }

 }

 if (fclose(diRfile) != 0) {
  printf("Can't close datafile %s. Exiting to system\n", filenm);
  exit(1);
 }
}

/*---------------------------------------------------------*/

/* diR_kludge: Attempt to match Cloudy's default dielectric recombination data:   */
/*             "The default condition is for the guestamates of third row and forth
	       row elements... dielectronic recombination coefficients presented by
	       Ali et al. (1991)... The code uses the means of the rate coefficients
	       for the four lowest stages of ionization of C, N, and O. These are 
	       3�2�10-13, 3�2 �10-12, 1.5�0.5�10-11, and 2.5�1.4 � 10-11 cm3 s-1 ...
	       These are used for those ions that have no rate coefficients because 
	       no better can be done at present."    hazy1_05_07.pdf Pg193        */

/*   Temp:     These rates are not given any temp dependence. I don't know why, and I
	       can't find what temp they were calculated at (10000K?). They were orig-
	       inally applied to the Paris "Model Nebulae" meeting's HII and planetary
	       models (Pequignot 1986) but I can't find the temp for those.       */

/*   Mg,Al,Si: For elements that have some data (currently Mg,Al,Si) it's not clear
	       if Cloudy is applying the kludge to their other ions. I've chosen not
	       to, since Section7 of NussbaumerStorey85 says e.g. that there is no low
	       temp dielec recomb for Ne-like ions to F-like ions ("the recombined 
	       ions MgII, AlIII and SiIV").                                       */

/*   Refs:     Ali,Blum,Bumgardner,Cranmer,Ferland,Haefner,&Tiede,1991,PASP 103,1182
               P�quignot,1986, Wordshop on Model Nebulae (l'Observatoire de Paris)p363
	       Nussbaumer & Storey,1986, A&AS 64, 545
	       (Ali took the C,N,O data from Nussbaumer & Storey,1983, A&A 126, 75) */

void diR_kludge(
 double **datarray,
 int sizeZ,
 int *Z,
 int *J)
{
 int i, j;
 int kludge;                    /* Flag whether to use the approximation */

 for (i = 1; i < sizeZ; ++i) {
  if (11 <= Z[i] && Z[i] <= 30) {       /* All 3rd or 4th row elements */

   kludge = 1;

   for (j = 1; j <= J[i]; ++j) {
    if (datarray[i][j] != 0.0) {        /* If any ion has rate data... */
     kludge = 0;                /* ...turn off the flag,       */
     break;                     /*(don't test the rest of the ions) */
    }
   }

   if (kludge == 1) {           /*If no ions had data, apply the approximation */
    datarray[i][1] = 3e-13;
    datarray[i][2] = 3e-12;
    datarray[i][3] = 1.5e-11;
    datarray[i][4] = 2.5e-11;
   }

  }
 }

}

/*------------------------------------------------------------- */
/*photofunc: Returns the value of the integrand, for calculating
  the rate of photoionisation of 'Z,numelecs' with a vacancy in 
  'shell'. Rate = (4pi * J_L / h) * integral wrt E, from E_ion to 
  10*E_ion (~inf), of [sigma*(1/E)*(E/E_L)^-1.5 ]              */

float photofunc(
 float E_erg,
 int Z,
 int numelecs,
 int shell,
 double *ColDen,
 double redshift)
{
 //For input to phfit2, ie. for sigma
 double E_eV = E_erg / 1.602e-12;       /* Change from float (in ergs) to double (in eV) */
 double sigma = 0.0;            /* photoionization cross section */
 phfit2_(&Z, &numelecs, &shell, &E_eV, &sigma);
 sigma *= 1.e-18;               /* From Mb to cm^2 */

 //USE SPECTRUM nu^-0.5
 return sigma * (1.0 / E_erg) * pow((E_erg / E_thresh[0]), -1.5);
}

/*-----------------------------*/
