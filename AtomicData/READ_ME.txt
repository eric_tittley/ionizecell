*  ct.f (and my versions giga_ct.f and my_ct.f)      scr 20/04/06
*  Charge transfer with hydrogen
These fortran functions are not stable for non-physical reactions,
e.g. if called for hydrogen or for (ionization from- or recombination to-) 
any fully ionized state.
Therefore MUST NOT loop over these functions for all available atoms and 
states

NB. [Most/All?] other fortran functions provided on Dima Verner's Atomic 
Data pages include checks, and return zero rates for unphysical calls.

*****************************************************************